import { body } from "express-validator/check";
import { authenticationMiddleware } from "../middleware/auth";
import {
  addText
} from "../language/validatorLanguage";
import {
    updateText
  } from "../language/validatorLanguage";
export const commonUpdateValidator = [
  authenticationMiddleware(true),
  body("name")
    .exists().withMessage(addText("name")),
];

export const commonDeleteValidator = [
  authenticationMiddleware(true),
  body("id")
    .exists().withMessage(addText("id")),
];

export const createValidator = [
  authenticationMiddleware(true),
  body("name")
    .exists().withMessage(addText("name")),
];