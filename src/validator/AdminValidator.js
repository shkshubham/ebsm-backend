import { body } from "express-validator/check";
import { authenticationMiddleware } from "../middleware/auth";
import {
  addText
} from "../language/validatorLanguage";
export const registerValidator = [
  authenticationMiddleware(false),
  body("email")
    .exists().withMessage(addText("email"))
    .isEmail().withMessage("please provide valid email"),
  body("password")
    .exists().withMessage(addText("password"))
    .isLength({ min: 5 }).withMessage("password must be at least 5 chars long"),
  body("name")
    .exists().withMessage(addText("name")),
];