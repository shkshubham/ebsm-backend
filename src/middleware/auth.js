export function authenticationMiddleware (condition) {
    return (req, res, next) => {
        if (!condition) {
            if (!req.isAuthenticated()) return next();
            return sendResponse(res, `You are already logged`);
        }
        if (req.isAuthenticated()) return next();
            return sendResponse(res, `Please log in`, 401);
    };
}
function sendResponse(res, message, _status = 400, data=null) {
    return res.status(_status).json({
        status: false,
        message,
        data,
    });
}