import app from "./routes/index";
require("dotenv").config();

app.listen(process.env.PORT || 3000, () => {
    console.log(`Listening to ${process.env.PORT}`);
});