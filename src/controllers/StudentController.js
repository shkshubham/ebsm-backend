import { Course, Class, Student } from "../models";
import {
    commonAddService, commonUpdateService, commonShowService,
    commonAllListService, commonDeleteService
} from "../services/commonServices";

const type = "student"
class StudentController {
    list(req, res) {
        return commonAllListService(req, res, Student, [], type);
    }
    create(req, res) {
        const {
            roll_no
        } = req.body;
        return commonAddService(req, res, Student, {
            roll_no
        }, type);
    }
    update(req, res) {
        const {
            id
        } = req.body;
        return commonUpdateService(req, res, Student, id, type);
    }
    show(req, res) {
        const include = [
            {
                model: Class,
                as: "class",
            },
            {
                model: Course,
                as: "course",
            }
        ];
        return commonShowService(req, res, Student, include, type);
    }
    delete(req, res) {
        return commonDeleteService(req, res, Student, type);
    }
}

export default StudentController;