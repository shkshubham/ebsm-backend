import { Course, Class, Student } from "../models";
import {
    commonAddService, commonUpdateService, commonShowService,
    commonAllListService, commonDeleteService
} from "../services/commonServices";

const type = "class"
class ClassController {
    list(req, res) {
        return commonAllListService(req, res, Class, [], type);
    }
    create(req, res) {
        const {
            name
        } = req.body;
        return commonAddService(req, res, Class, {
            name
        }, type);
    }
    update(req, res) {
        const {
            id
        } = req.body;
        return commonUpdateService(req, res, Class, id, type);
    }
    show(req, res) {
        const include = [
            {
                model: Student,
                as: "students",
            },
            // {
            //     model: Course,
            //     as: "courses",
            // }
        ];
        return commonShowService(req, res, Class, include, type);
    }
    delete(req, res) {
        return commonDeleteService(req, res, Class, type);
    }
}

export default ClassController;