import { Course, Class } from "../models";
import {
    commonAddService, commonUpdateService, commonShowService,
    commonAllListService, commonDeleteService
} from "../services/commonServices";

const type = "course"
class CourseController {
    list(req, res) {
        return commonAllListService(req, res, Course, [], type);
    }
    create(req, res) {
        const {
            name
        } = req.body;
        return commonAddService(req, res, Course, {
            name
        }, type);
    }
    update(req, res) {
        const {
            id
        } = req.body;
        return commonUpdateService(req, res, Course, id, type);
    }
    show(req, res) {
        const include = [
            {
                model: Class,
                as: "classes",
            }
        ];
        return commonShowService(req, res, Course, include, type);
    }
    delete(req, res) {
        return commonDeleteService(req, res, Course, type);
    }
}

export default CourseController;