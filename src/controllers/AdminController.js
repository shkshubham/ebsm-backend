import { Admin } from "../models";
import {
    commonRegisterService, commonProfileService, commonLogoutService,
} from "../services/authCommonServices";
class AdminController {
    registerAdmin(req, res) {
        return commonRegisterService(req, res, Admin, "Admin");
    }
    adminProfile(req, res) {
        const {
            id
        } = req.user;
        return commonProfileService(req, res, Admin, [
        ], "Admin");
    }
    logoutAdmin(req, res) {
       return commonLogoutService(req, res, "Admin");
    }
}

export default AdminController;