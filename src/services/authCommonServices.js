import { jsonResponse, setValidator } from "../helpers/helper";
import uuidv4 from "uuid/v4";
import { generateHashPassword } from "../helpers/helper";

export function commonRegisterService(req, res, Model, type) {
    setValidator(req, res);
    const {
        email,
        password
    } = req.body;
    const createData = req.body;
    createData.id = uuidv4();
    createData.password = generateHashPassword(password);
    Model
    .findOrCreate({
        where: {
            email
        },
        defaults: createData
    })
    .spread((modelData, created) => {
        if (created === true) {
            return jsonResponse(res, true, `${type} registered`, {}, 201);
        }
        return jsonResponse(res, false, `${type} already register`);
    }).catch((error) => {
        console.log(error);
        return jsonResponse(res, false, "error", error);
    });
}

export function commonProfileService(req, res, Model, includeList, _type) {
    const {
        id,
    } = req.user;
        return Model.findOne({
            where: {
                id
            },
            include: includeList
        }).then((foundData) => {
            console.log(foundData)
            return jsonResponse(res, true,  `${_type} profile`, foundData);
        });
}

export function commonLogoutService(req, res, _type) {
    const {
        type
    } = req.user;
    return req.session.destroy(function (err) {
        if (err) {
            return jsonResponse(res, false, err);
        }
        return jsonResponse(res, true, `${type} logout out successfully`);
    });
}