import { jsonResponse, setValidator } from "../helpers/helper";
import uuidv4 from "uuid/v4";

export function commonUpdateService(req, res, Model, id, type) {
    setValidator(req, res);
    Model.findOne({
        where: {
            id
        }
    }).then((foundData) => {
        let updateData = req.body;
        if (updateData.id) {
            delete updateData.id;
        }
        if (updateData.user_id) {
            delete updateData.user_id;
        }
        if (updateData.password) {
            delete updateData.password;
        }
        if (updateData.email) {
            delete updateData.email;
        }
        foundData.update(updateData).then(() => {
            return jsonResponse(res, true, `${type} updated`);
        });
    }).catch((err) => {
        console.log(error);
        return jsonResponse(res, false, `${type} not found`, err);
    });
}

export function commonAddService(req, res, Model, condition, type) {
    setValidator(req, res);
    let reqData = req.body;
    reqData.id = uuidv4();
    Model
    .findOrCreate({
        where: condition,
        defaults: reqData
    })
    .spread((foundData, created) => {
        if (created === true) {
            return jsonResponse(res, true, `${type} added` , {}, 201);
        }
        return jsonResponse(res, false, `${type} already available`);
    }).catch((error) => {
        console.log(error);
        return jsonResponse(res, false, "error", error);
    });
}

export function commonAllListService(req, res, Model, include, type) {
    Model.findAll({
        include,
        order: [
            ["updatedAt", "DESC"],
        ],
    }).then((foundData) => {
        return jsonResponse(res, true, `all ${type}`, foundData);
    }).catch((err) => {
        console.log(err);
        return jsonResponse(res, false, "failed", err);
    });
}

export function commonShowService(req, res, Model, include, type) {
    const {
        id
    } = req.params;
    console.log('--==', include)
    Model.findOne({
        where: {
            id
        },
        include
    }).then((foundData) => {
        return jsonResponse(res, true, `${type}`, foundData);
    }).catch((err) => {
        console.log(err);
        return jsonResponse(res, false, "failed", err);
    });
}
export function commonDeleteService(req, res, Model, type) {
    setValidator(req, res);
    const {
        id
    } = req.body;
    Model.findOne({
        where: {
            id
        },
    }).then((foundData) => {
        foundData.destroy().then(() => {
            return jsonResponse(res, true, `${type} deleted`);
        });
    }).catch((err) => {
        console.log(err);
        return jsonResponse(res, false, "failed", err);
    });
}