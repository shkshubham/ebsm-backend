import { validationResult } from "express-validator/check";
import fs from "fs";
import bcrypt from "bcrypt-nodejs";

export function jsonResponse(res, status, message, data = null, statusCode= null) {
    const varStatus = statusCode ? statusCode : status ? 200 : 200;
    res.status(varStatus).json({
        status,
        message,
        data
    });
}

export function initValidator(req, res) {
    const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
        return {
            error: param,
            msg
        };
      };
    const errors = validationResult(req).formatWith(errorFormatter);
    if (!errors.isEmpty()) {
       return errors.array({ onlyFirstError: true });
    }
    return null;
}

export function setValidator(req, res) {
    const errors = initValidator(req, res);
    if (errors) {
        return jsonResponse(res, false, "failed", errors, 400);
    }
}

export function getConfig() {
    const env       = process.env.NODE_ENV || "development";
    const rewConfig = fs.readFileSync("config/config.json");
    const configParse = JSON.parse(rewConfig);
    return configParse[env];
}
export function generateHashPassword(password) {
    const saltRounds = 10;
    const myPlaintextPassword = password;
    const salt = bcrypt.genSaltSync(saltRounds);
    const passwordHash = bcrypt.hashSync(myPlaintextPassword, salt);
    return passwordHash;
}