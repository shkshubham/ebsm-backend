export default (sequelize, DataTypes) => {
  const Course = sequelize.define('Course', {
    id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
      }
  }, {
    timestamps: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
    }
  });
  Course.associate = function(models) {
    const {
      Class
    } = models;
    Course.hasMany(Class, { as: "classes", foreignKey: "course_id" });
    // associations can be defined here
  };
  return Course;
};