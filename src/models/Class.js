export default (sequelize, DataTypes) => {
  const Class = sequelize.define("Class", {
    id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    course_id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, 
  {
    timestamps: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ["course_id", "createdAt", "updatedAt", "deletedAt"] },
    }
  });

  Class.associate = function(models) {
    const {
      Course,
      Student
    } = models;
    Class.belongsTo(Course, { as: "courses", foreignKey: "course_id" });
    Class.hasMany(Student, { as: "students", foreignKey: "class_id" });
    // associations can be defined here
  };
  return Class;
};