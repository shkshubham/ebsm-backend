export default (sequelize, DataTypes) => {
  const Student = sequelize.define('Student', {
    id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    roll_no: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    course_id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
    },
    class_id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
    }
  }, {
    timestamps: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ["createdAt", "updatedAt", "deletedAt", "course_id", "class_id"] },
    }
  });
  Student.associate = function(models) {
    const {
      Class,
      Course
    } = models;
    Student.belongsTo(Class, { as: "class", foreignKey: "class_id" });
    Student.belongsTo(Course, { as: "course", foreignKey: "course_id" });
    // associations can be defined here
  };
  return Student;
};