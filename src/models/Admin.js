export default (sequelize, DataTypes) => {
  const Admin = sequelize.define("Admin", {
    id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true,
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        min: 2
      }
    }
  } , {
    defaultScope: {
      attributes: { exclude: ["password", "createdAt", "updatedAt"] },
    }
  });

  Admin.associate = function(models) {
    // associations can be defined here
  };
  return Admin;
};