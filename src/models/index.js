import fs from "fs";
import path from "path";
import Sequelize from "sequelize";
require("dotenv").config();
import { getConfig } from "../helpers/helper";
const basename  = path.basename(__filename);
let config = getConfig();
const db = {};
let sequelize;
const {
  host,
  dialect
} = config;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, {
    host,
    dialect,
    logging: ((log) => {
      console.log(log);
    })
  });
}

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });
fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf(".") !== 0) && (file !== basename) && (file.slice(-3) === ".js");

  })
  .forEach(file => {
    const model = sequelize["import"](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});
db.sequelize = sequelize;
db.Sequelize = Sequelize;

export const {
  Admin,
  Course,
  Class,
  Student
} = db;
export const SequelizeOp = Sequelize.Op;