import { authenticationMiddleware } from "../middleware/auth";
import ClassController from "../controllers/ClassController";
import {
    createValidator, commonUpdateValidator, commonDeleteValidator
} from '../validator/CommonValidator'
const classController = new ClassController();

export default (app) => {
    app.get(`/list`, authenticationMiddleware(true), classController.list);
    app.post(`/create`, createValidator, classController.create);
    app.get(`/show/:id/`, authenticationMiddleware(true), classController.show);
    app.patch(`/update`, commonUpdateValidator, classController.update);
    app.delete(`/delete`, commonDeleteValidator, classController.delete);
    return app;
};