import passport from 'passport';
import { authenticationMiddleware } from "../middleware/auth";
import AdminController from "../controllers/AdminController";
import { registerValidator } from "../validator/AdminValidator";
const adminController = new AdminController();

export default (app) => {
    app.post(`/register`, registerValidator , adminController.registerAdmin);
    app.post("/login",
    [
        authenticationMiddleware(false),
        passport.authenticate("admin-local", {failureRedirect: "/loginFailed"})
    ],
    function(req, res) {
        res.json({
            status: true,
            message: `Admin Logged In`
        });
    });
    app.get(`/profile`, authenticationMiddleware(true), adminController.adminProfile);
    app.get(`/logout`, authenticationMiddleware(true), adminController.logoutAdmin);
    return app;
};