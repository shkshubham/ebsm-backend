import express from "express";
import bodyParser from "body-parser";
import session from "express-session";
import uuid from "uuid/v4";
import expressValidator from  "express-validator";
import passport from "passport";
import morgan from "morgan";
import compression from "compression";
import cookiePasser from "cookie-parser";
import { getConfig } from "../helpers/helper";
import ApiURL from "./api";

require("../authenticate/passport");

const app = express();
const {
  host,
  password,
  database,
  username,

} = getConfig();
const MySQLStore = require("express-mysql-session")(session);
const options = {
  host,
  port: 3306,
  user: username,
  password,
  database
};
const sessionStore = new MySQLStore(options);

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", req.get("origin"));
  res.header("Access-Control-Allow-Credentials", "true");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  return next();
});
app.use(cookiePasser());
app.use(compression());
app.use(morgan(":method :url | status = :status | res-length = :res[content-length] | response-time = :response-time ms | referrer = :referrer | user-agent = :user-agent"));
app.use(express.json());
app.use(expressValidator());

app.use(session({
    genid: (req) => {
      // console.log(req.user);
      return uuid();
    },
    store: sessionStore,
    secret: process.env.EXPRESS_SESSION_SECRET || "qwertyuiop",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

const api = `/api`;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.get("/loginFailed", (req, res) => {
  return res.status(400).json({
    status: false,
    message: "invalid credentials"
  });
});


app.get(`${api}/ping`, (req, res) => {
  if (req.user) {
    const {
      email,
      name,
     } = req.user;
       return res.status(200).json({
         status: true,
         message: "Your are logged in",
         data: {
           email,
           name
         }
       });
  }
  return res.status(401).json({
    status: false,
    message: "Your are not logged in",
  });
});
ApiURL.forEach((routeDetail) => {
  const {
    url,
    route,
  } = routeDetail.route(express.Router());
    app.use(`${api}/${url}/`, route);
});

export default app;