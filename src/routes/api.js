import adminRoute from './admin';
import courseRoute from './course';
import classRoute from './class';
import studentRoute from './student';

export default [
    {
       route: (router) => {
            return {
                url: "admin",
                route: adminRoute(router),
            };
       }
    },
    {
        route: (router) => {
             return {
                 url: "course",
                 route: courseRoute(router),
             };
        }
     },
     {
        route: (router) => {
             return {
                 url: "class",
                 route: classRoute(router),
             };
        }
     },
     {
         route: (router) => {
              return {
                  url: "student",
                  route: studentRoute(router),
              };
         }
    }
];

