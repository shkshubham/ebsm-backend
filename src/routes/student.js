import { authenticationMiddleware } from "../middleware/auth";
import StudentController from "../controllers/StudentController";
import {
    createValidator, commonUpdateValidator, commonDeleteValidator
} from '../validator/CommonValidator'
const studentController = new StudentController();

export default (app) => {
    app.get(`/list`, authenticationMiddleware(true), studentController.list);
    app.post(`/create`, createValidator, studentController.create);
    app.get(`/show/:id/`, authenticationMiddleware(true), studentController.show);
    app.patch(`/update`, commonUpdateValidator, studentController.update);
    app.delete(`/delete`, commonDeleteValidator, studentController.delete);
    return app;
};