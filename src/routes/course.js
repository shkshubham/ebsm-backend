import { authenticationMiddleware } from "../middleware/auth";
import CourseController from "../controllers/CourseController";
import {
    createValidator, commonUpdateValidator, commonDeleteValidator
} from '../validator/CommonValidator'
const courseController = new CourseController();

export default (app) => {
    app.get(`/list`, authenticationMiddleware(true), courseController.list);
    app.post(`/create`, createValidator, courseController.create);
    app.get(`/show/:id/`, authenticationMiddleware(true), courseController.show);
    app.patch(`/update`, commonUpdateValidator, courseController.update);
    app.delete(`/delete`, commonDeleteValidator, courseController.delete);
    return app;
};