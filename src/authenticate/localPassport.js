import { Strategy as LocalStrategy } from "passport-local";
import bcrypt from "bcrypt-nodejs";

export default (Model) => {
    return new LocalStrategy(
    function(username, password, done) {
    Model.findOne({
        where: { email: username},
        attributes: ["id", "email", "password"]
      }).then((foundData) => {
        if (!foundData) {
          return done(null, false);
        }
        bcrypt.compare(password, foundData.password, (err, matched) => {
          if (!matched) {
            return done(null, false);
          }
          return done(null, foundData.id);
        });
      }).catch((err) => {
        if (err) {
          return done(err);
        }
      });
    }
  );
};