import passport from "passport";
import { Admin } from "../models";
import LocalPassport from "./localPassport";
passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((id, done) => {
    let data = commonDeserialize(id, Admin, done);
    return data;
});

passport.use("admin-local", LocalPassport(Admin));

export function commonDeserialize (_id, Model, done) {
    Model.findOne({
      where: {
        id: _id
      }
    }).then((found) => {
      const {
        id,
        name,
      } = found;
      done(null, {
        id,
        name,
      });
  });
}